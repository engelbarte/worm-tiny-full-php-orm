<?php

require_once "Table.php";

/*
 * Try/catch car lors de la création de l'objet on vérifie les fichiers de conf
 */

try {
    $table = new Table();
} catch (Exception $e) {
    echo "Error message : " . $e->getMessage() . "\n";
}

/*
 * Petite demo des features de la classe :
 *  - Création de table : nécessite un fichier de conf en json, ici on va utiliser test.json (qui n'est pas prit par la migration
 *    car n'a pas le nom adéquat.
 *  - Describe : affiche une description de la table
 */

// Création de table
$table->create("test.json");
$table->describe("tests");

// Ajout de colonne
$table->add_column("tests", "nouvelle_colonne", "int", "not null");
$table->add_column("tests", "jolie_colonne", "varchar(255)", "");
$table->describe("tests");

// Suppression de colonne
$table->delete_column("tests", "nouvelle_colonne");
$table->describe("tests");

// Modification de colonne
$table->modify_column("tests", "jolie_colonne", "int", "not null");
$table->describe("tests");

// Ajout / Suppression de clés primaires
$table->drop_primary_key("tests");
$table->add_primary_key("tests", "jolie_colonne");
$table->describe("tests");

// Renommage de colonne
$table->rename_column("tests", "jolie_colonne", "belle_colonne", "int", "not null");
$table->describe("tests");

$table->drop_table("tests");

/*
 * La commande run migration permet de lancer les requêtes SQL à l'aide des méthodes vues plus haut, le tout
 * en suivant les instructions des fichiers de conf
 */

$table->run_migration();