<?php

require_once "Post.php";

$post = new Post();
$post_deux = new Post();

$post->content = "Bonjour je suis du contenu";
$post->save();

$post_deux->content = "Un autre contenu";
$post_deux->save();

// Chargement d'un post depuis la base, mise à jour et save
$post->load(2);
$post->content = "Oh j'ai changé";
$post->save();

// Trouve le(s) post(s) avec le prédicat, si seul post récupéré alors renvoi un objet sinon un array
$post::find("content like '%ws%'", false);

// Trouve le(s) post(s) avec le prédicat, renvoi toujours un array de d'objets
$post::find("content like '%CA M%'");