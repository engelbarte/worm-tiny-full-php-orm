<?php

require_once "Mysql_Connection.php";

class Table
{
    public $conf_path;
    public $files_list;
    public $my_connection;

    /**
     * Table constructor.
     * On charge les fichiers et on vérifie qu'ils sont valides (json, et certains champs)
     * Si okay on met en place la connexion au server SQL
     */
    public function __construct() {
        $this->conf_path = "config/";
        $this->init_files_list();
        $this->validate_files();
        $instance_tmp = new Mysql_Connection();
        $this->my_connection = $instance_tmp::getInstance();
    }

    /**
     * On scanne le dossier contenant les fichiers de config
     * On stock un tableau contenant les noms de ces fichiers dans $files_list
     *
     * Ne prend que les fichiers contenant install_ ou update_ dans leur nom
     */
    public function init_files_list() {
        $files = scandir($this->conf_path, SCANDIR_SORT_DESCENDING);

        foreach ($files as $key => $value) {
            if (!strstr($value, "install_") && !strstr($value, "update_")) {
                unset($files[$key]);
            }
        }

        sort($files);
        $this->files_list = $files;
    }

    /**
     * On a deux types de fichiers dans config :
     *  - fichier d'installation : install_tablename.json
     *  - fichiers de mise à jour : update_tablename-1.json
     *
     * Un seul fichier d'installation
     * Plusieurs fichiers de mise à jour, numérotés dans l'ordre
     * (Je me suis inspiré du fonctionnement de Magento 1, sans les numéros de version, etc)
     *
     */
    public function validate_files() {
        // validation des noms
        $is_an_install = false;

        foreach ($this->files_list as $file) {
            if (strstr($file, "install_")) {
                $is_an_install = true;
            } else {
                if (!is_numeric(substr($file, strlen($file) - 6, 1))) {
                    throw new Exception(" missing number in " . $file . ", exemple : update_post-6.json");
                }
            }
        }

        if (!$is_an_install) {
            throw new Exception(" installation file is missing");
        }

        // validation du contenu
        foreach ($this->files_list as $file) {
            if (strstr($file, "install")) {
                $this->validate_install_file($file);
            } else {
                $this->validate_update_file($file);
            }
        }
    }

    /**
     * @param $file : nom du fichier à valider
     * @throws Exception : quand il y a un problème dans le json du fichier
     */
    public function validate_install_file($file) {
        $json = json_decode(file_get_contents($this->conf_path . $file), true);
        if (is_null($json) || !$json) {
            throw new Exception("json is corrupted in " . $file);
        }
        if (!array_key_exists('fields', $json)) {
            throw new Exception("in " . $file . " no fields found");
        }
        foreach ($json['fields'] as $column) {
            if (!array_key_exists('name', $column)
                || !array_key_exists('type', $column)
                || !array_key_exists('property', $column)) {
                throw new Exception("in " . $file . " fields, problem with rows");
            }
        }
    }

    /**
     * @param $file : nom du fichier à valider
     * @throws Exception : quand il y a un problème dans le json du fichier
     */
    public function validate_update_file($file) {
        /*
         * TODO : Meilleure gestion d'erreur des fichiers d'update, pour l'instant des erreurs possibles en fonction des attributs dans le json
         */
        $actions = array("add_column", "delete_column", "modify_column", "rename_column", "add_primary_key", "drop_primary_key", "drop_table");
        $json = json_decode(file_get_contents($this->conf_path . $file), true);
        if (is_null($json) || !$json) {
            throw new Exception("json is corrupted in " . $file);
        }
        if (!array_key_exists('actions', $json)) {
            throw new Exception("in " . $file . " no actions found");
        }
        foreach ($json['actions'] as $column) {
            if (!array_key_exists('name', $column)
                || !array_key_exists('action', $column)) {
                throw new Exception("in " . $file . " actions, missing name or action");
            }
            if (!in_array($column['action'], $actions)) {
                throw new Exception("in " . $file . " unknown action : " . $column['action']);
            }
        }
    }

    /**
     * Renvoi le nom de la table contenu dans le fichier install_tablename.json SI EXISTE
     * SINON renvoi le nom de la table généré avec le nom du fichier
     *
     * @param $install_file : nom du fichier contentant les infos de la table
     * @return string
     */
    public function get_table_name($install_file) {
        $json = json_decode(file_get_contents( $this->conf_path . $install_file), true);

        if (array_key_exists('table_name', $json)) {
            $table_name = $json['table_name'];
        } else {
            if (strstr($install_file, "install")) {
                $table_name = substr($install_file, 0,  strlen($install_file) - 5);
                $table_name = substr($table_name, 8);
            } else {
                $table_name = substr($install_file, 0,  strlen($install_file) - 5);
                $table_name = substr($table_name, 9);
            }
        }

        return $table_name;
    }

    /**
     * Lance une migration, mais qu'est ce qu'une migration me direz vous ?
     * Une migration lit les fichiers dans config/ (qui on le bon nom)
     * Et execute les requêtes SQL en fonction des informations dans le JSON
     */
    public function run_migration() {
        foreach ($this->files_list as $file) {
            if (strstr($file, "install")) {
                $this->create($file);
            } else {
                $this->alter($file);
            }
        }
    }

    /**
     * Crée une table
     *
     * @param $install_file : nom du fichier contentant les infos de la table
     * @param $drop_table_before true = drop table avant la création
     */
    public function create($install_file, $drop_table_before = true) {
        $json = json_decode(file_get_contents( $this->conf_path . $install_file), true);
        $table_name = $this->get_table_name($install_file);

        if ($drop_table_before) {
            $query_sql = "DROP TABLE IF EXISTS " . $table_name . "; CREATE TABLE " . $table_name . " (" ;
        } else {
            $query_sql = "CREATE TABLE " . $table_name . " (" ;
        }

        $i = 0;
        $number_of_columns = count($json['fields']);
        foreach ($json['fields'] as $jon) {
            $i++;
            $query_sql .= $jon['name'] . " " . $jon['type'] . " " . $jon['property'] . ($i < $number_of_columns ? ", " : ")");
        }

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Fonction appelée par la migration lorsqu'il s'agit d'un fichier de mise à jour
     *
     * @param $update_file nom du fichier de mise a jour
     */
    public function alter($update_file) {
        $class_name = "Table";
        $json = json_decode(file_get_contents( $this->conf_path . $update_file), true);
        $table_name = $this->get_table_name($update_file);

        foreach ($json["actions"] as $action) {
            $function_to_call = array($class_name, $action['action']);
            $parameters = $action;
            unset($parameters['action']);
            array_unshift($parameters, $table_name);

            call_user_func_array($function_to_call, $parameters);
        }
    }

    /**
     * Supprime une table
     *
     * @param $table_name
     */
    public function drop_table($table_name) {
        $query_sql = "DROP TABLE IF EXISTS " . $table_name;

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Ajoute une colonne dans la table
     *
     * @param $table_name
     * @param $column_name
     * @param $column_type
     * @param $column_property
     */
    public function add_column($table_name, $column_name, $column_type, $column_property) {
        $query_sql = "ALTER TABLE " . $table_name . " ADD " .$column_name . " " . $column_type . " " . $column_property;

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Supprime une colonne donnée dans une table donnée
     *
     * @param $table_name
     * @param $column_name
     */
    public function delete_column($table_name, $column_name) {
        $query_sql = "ALTER TABLE " . $table_name . " DROP " .$column_name;

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Modifie une colonne donnée dans une table donnée
     *
     * @param $table_name
     * @param $column_name
     * @param $column_type
     * @param $column_property
     */
    public function modify_column($table_name, $column_name, $column_type, $column_property) {
        $query_sql = "ALTER TABLE " . $table_name . " MODIFY " .$column_name . " " . $column_type . " " . $column_property;

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Renomme une table en une autre
     *
     * @param $table_name
     * @param $old_column_name
     * @param $new_column_name
     * @param $column_type
     * @param $column_property
     */
    public function rename_column($table_name, $old_column_name, $new_column_name, $column_type, $column_property) {
        $query_sql = "ALTER TABLE " . $table_name . " CHANGE " .$old_column_name
            . " " . $new_column_name . " " . $column_type . " " . $column_property;

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Ajoute une clé primaire sur la colonne donnée dans la table donnée
     *
     * @param $table_name
     * @param $column_to_primary
     */
    public function add_primary_key($table_name, $column_to_primary) {
        $query_sql = "ALTER TABLE " . $table_name . " ADD PRIMARY KEY (" . $column_to_primary . ")";

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Supprime la clé primaire de la table donnée
     *
     * @param $table_name
     */
    public function drop_primary_key($table_name) {
        $query_sql = "ALTER TABLE " . $table_name . " DROP PRIMARY KEY";

        $this->my_connection->getConnection()->exec($query_sql);
    }

    /**
     * Affiche la table donnée
     *
     * @param $table_name
     */
    public function describe($table_name) {
        $query_sql = "DESCRIBE " . $table_name;

        $fetched = $this->my_connection->getConnection()->query($query_sql)->fetchAll();

        echo "==================================================\n";
        echo "TABLE : " . $table_name . "\n";

        foreach ($fetched as $column) {
            echo "FIELD : " . $column["Field"] . " | TYPE : " . $column["Type"] . " | NULL ? "
                . $column["Null"] . " | KEY ? " . ($column["Key"] === "" ? "NO" : $column["Key"]) . "\n";
        }

        echo "==================================================\n";
    }
}