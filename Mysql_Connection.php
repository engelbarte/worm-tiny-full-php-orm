<?php

class Mysql_Connection
{

    const DEFAULT_USER = "admin";
    const DEFAULT_HOST = "localhost";
    const DEFAULT_PSWD = "admin";
    const DEFAULT_DBNAME = "ORM";

    private $pdo_instance = null;

    private static $mysql_instance = null;

    public function __construct()
    {
        $this->pdo_instance = new PDO('mysql:dbname='.self::DEFAULT_DBNAME.';host='.self::DEFAULT_HOST, self::DEFAULT_USER, self::DEFAULT_PSWD);
    }

    public static function getInstance()
    {
        if (is_null(self::$mysql_instance)) {
            self::$mysql_instance = new Mysql_Connection();
        }
        return self::$mysql_instance;
    }

    public  function getConnection() {
        return $this->pdo_instance;
    }
}