# Worm - A tiny (Wonder)full(?) php orm

## 1. Paramétrer la base de donnée : 

Dans le fichier Mysql_Connection.php : 

```php
    const DEFAULT_USER = "admin";       // Utilisateur pour se connecter à la base
    const DEFAULT_HOST = "localhost";   // Host pour la base, laisser localhost si base en local
    const DEFAULT_PSWD = "admin";       // Mot de passe de l'utilisateur choisi
    const DEFAULT_DBNAME = "ORM";       // Nom de la base de donnée à utiliser
```

## 2. Les classes Entity et Post

Ces classes permettent de manipuler des Posts dans la base de donnée. Ils ont été fait en TP et ne présentent pas de feature particulière.

Un exemple des possibilités se situe dans le fichier run.php

## 3. La classe Table

La classe Table permet de gérer les migrations.
Un exemple des possibilités se situe dans le fichier run_table.php

Dans le dossier config/ il y a les fichiers JSON permettant de paramétrer la migration. 

Il y a deux types :
- Fichier d'installation, format du nom : install_tablename.json
- Fichier de mise à jour, format du nom : update_tablename-numerodemaj.json (numerodemaj de 1 à 9)

### Format JSON du fichier d'installation

```json
{      
  "table_name": "posts",     
  "fields": {     
    "1": {     
      "name": "id",
      "type": "int",
      "property": "PRIMARY KEY NOT NULL"
    },
    "2": {
      "name": "content",
      "type": "varchar(255)",
      "property": ""
    },
    "3": {
      "name": "subcontent",
      "type": "varchar(255)",
      "property": ""
    }
  }
}
```

Le table_name est optionnel

### Format JSON d'un fichier de mise à jour

```json
{
  "table_name": "posts",
  "actions": {
    "1": {
      "action": "add_column",
      "name": "nouvelle",
      "type": "varchar(255)",
      "property": ""
    },
    "2": {
      "action": "modify_column",
      "name": "nouvelle",
      "type": "int",
      "property": ""
    }
  }
}
```

Les actions possibles par mise à jour :

- Suppression de la table :
    - Nom d'action : drop_table
- Ajout de colonne :
    - Nom d'action : add_column
    - Attributs nécessaires :
        - name : nom de la colonne
        - type : type de la colonne
        - property : propriétés de la colonne (primary key, not null ...)
- Modification d'une colonne :
    - Nom d'action : modify_column
    - Attributs nécessaires :
        - name : nom de la colonne
        - type : type de la colonne
        - property : propriétés de la colonne (primary key, not null ...)
- Renommage d'une colonne :
    - Nom d'action : rename_column
    - Attributs nécessaires :
        - name : nom de la colonne avant
        - new_name : nom de la colonne apres
        - type : type de la colonne
        - property : propriétés de la colonne (primary key, not null ...)
- Suppression d'une colonne :
    - Nom d'action : delete_column
    - Attributs nécessaires :
        - name : nom de la colonne
- Ajout d'une clé primaire :
    - Nom d'action : add_primary_key
    - Attributs nécessaires :
        - name : nom de la colonne sur laquelle on veut mettre la clé
- Suppression de la clé primaire :
    - Nom d'action : drop_primary_key

Quelques bugs persistent : 

- Je ne gère pas les fichiers de maj au dessus de 9 (au dessus de 1 caractère pour le numéro de maj)
- Certaines fois si on ne met pas les bons arguments dans le JSON d'une action ça peut planter
    (Je ne vérifie pas encore tout en fonction des actions, etc ...)
- ...