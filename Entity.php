<?php

require_once "Entity_Interface.php";
require_once "Mysql_Connection.php";

abstract class Entity implements Entity_Interface
{
    static protected $table_name = NULL;
    protected $reflection;
    protected $my_connection;

    public function __construct()
    {
        $this->reflection = new ReflectionClass($this);
        $instance_tmp = new Mysql_Connection();
        $this->my_connection = $instance_tmp::getInstance();
    }

    /**
     * sauvegarde un élément en base de donnée
     *  - le crée si n'existe pas
     *  - le met à jour si existe
     */
    public function save()
    {
        try {
            // Récupération du nom de la table
            $table_name = self::get_table_name();

            // Tableau qui contiendra les attributs que l'on à besoin pour la création de l'élément en base
            $table_properties = array();

            // On parcours nos attributs publics
            foreach ($this->reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $prop) {
                // Récupération du nom
                $property_name = $prop->getName();

                // Si ce n'est pas l'id on le recupère dans notre tableaux
                if ($property_name !== "id") {
                    $table_properties[] = '`' . $property_name . '` = "' .$this->{$property_name} . '"';
                } else {
                    // Sinon on le stock
                    $id = $this->{$property_name};
                }
            }

            // Si l'id n'existe pas, c'est une création
            if (is_null($id)) {
                $query_sql = "INSERT INTO " . strtolower($table_name) . " SET " . implode($table_properties);
            } else {
                // Sinon on met à jour l'élément avec l'id donné
                $query_sql = "UPDATE " . strtolower($table_name) . " SET " . implode($table_properties) . " WHERE id = " . $id;
            }

            // On execute notre commande SQL
            $this->my_connection->getConnection()->exec($query_sql);
        } catch (ReflectionException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * permet de charger un post depuis la base de donnée
     *
     * @param $id
     */
    public function load($id) {

        try {
            $table_name = self::get_table_name();

            $query_sql = "SELECT * FROM " . $table_name . " WHERE id = " . $id;

            $result = $this->my_connection->getConnection()->query($query_sql)->fetch(PDO::FETCH_ASSOC);

            foreach ($this->reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
                $propertyName = $property->getName();
                $property->setValue($this, $result[$propertyName]);
            }
        } catch (ReflectionException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $predicat, exemple : "content like '%le m%'"
     * @param $only_array bool si false, renvoi un objet Post quand un seul
     * @return array|Post tableau contenant des objets, ou un seul objet
     * @throws Exception
     */
    public static function find($predicat, $only_array = true) {
        $array_posts = array();

        $query_sql = "SELECT * FROM " . self::get_table_name() . " WHERE ".  $predicat;

        $result = Mysql_Connection::getInstance()->getConnection()->query($query_sql)->fetchAll(PDO::FETCH_ASSOC);

        if (count($result) !== 1 || $only_array) {
            foreach ($result as $item) {
                $post = new Post();
                $post->load($item['id']);
                array_push($array_posts, $post);
            }
            return $array_posts;
        } else {
            $post = new Post();
            $post->load($result[0]['id']);
            return $post;
        }
    }

    /**
     * Renvoi le nom de la table, en fonction de l'attribut table_name de la classe, sinon via la reflectionClass
     *
     * @return null|string
     * @throws ReflectionException
     */
    public static function get_table_name() {
        $class = new ReflectionClass(get_called_class());
        return NULL !== static::$table_name ? static::$table_name : strtolower($class->getName());
    }
}